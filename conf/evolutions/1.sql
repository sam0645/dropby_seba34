
# --- !Ups
CREATE TABLE `Question` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`title`	TEXT NOT NULL,
	`detail`	TEXT,
	`date`	NUMERIC,
	`username`	TEXT NOT NULL,
	`totalAnswers`	INTEGER DEFAULT 0,
	`views`	INTEGER DEFAULT 0,
	`likes`	INTEGER DEFAULT 0,
	`userId`	INTEGER
);

CREATE TABLE `Answer` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`quesId`	INTEGER NOT NULL,
	`response`	TEXT NOT NULL,
	`date`	NUMERIC,
	`likes`	INTEGER DEFAULT 0,
	`dislikes`	INTEGER DEFAULT 0,
	`username`	TEXT,
	`userId`	INTEGER
);
--
-- CREATE TABLE `Tag` (
-- 	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
-- 	`name`	TEXT NOT NULL
-- );
CREATE TABLE `Tag` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT NOT NULL,
	`count`	INTEGER DEFAULT 1
);

CREATE TABLE `QuestionTag` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`quesId`	INTEGER NOT NULL,
	`tagId`	INTEGER NOT NULL
);

CREATE TABLE `User` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`username`	TEXT NOT NULL,
	`pword`	TEXT NOT NULL,
	`email`	TEXT NOT NULL
);


# --- !Downs
DROP TABLE Question;
DROP TABLE Answer;
DROP TABLE Tag;
DROP TABLE QuestionTag;
DROP TABLE User;