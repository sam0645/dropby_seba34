/**
 * Created by enricogiga on 13/06/2015.
 */


$(document).ready( function(){
       /** $("#voteUp").click(function(){
            voteUp();

        });


        $("#voteDown").click(function(){
            voteDown();

        });

        **/
    }
);


function voteUp(answId){


    jsRoutes.controllers.AllQuestions.likeAnswer(answId).ajax( {
        success : function ( data ) {

            $('#voteUp'+answId).addClass('disabled');
            var num = parseInt($('#numLikes'+answId).text()) +1;
            $('#numLikes'+answId).text(num);
        }
    });

}

function voteDown(answId){
    jsRoutes.controllers.AllQuestions.unlikeAnswer(answId).ajax( {
        success : function ( data ) {

            $('#voteDown'+answId).addClass('disabled');
            var num = parseInt($('#numUnlikes'+answId).text()) +1;
            $('#numUnlikes'+answId).text(num);
        }
    });
}


function likeQuestion(quesId){
    jsRoutes.controllers.AllQuestions.likeQuestion(quesId).ajax( {
        success : function ( data ) {

            $('#likeQuestion'+quesId).addClass('disabled');
           var num = parseInt($('#quesLikes'+quesId).text()) +1;
            $('#quesLikes'+quesId).text(num);
        }
    });
}