/**
 * Created by CheeZee on 2015/6/5.
 */
    $(document).ready( function(){
        var words = [
            {text: "SEBA", weight: 13, link: '#'},
            {text: "Informatik", weight: 10.5, link: '#'},
            {text: "WebDev", weight: 9.4, link: '#'},
            {text: "Play", weight: 8},
            {text: "Angular", weight: 6.2},
            {text: "Recommendation", weight: 5},
            {text: "Party", weight: 5},
            /* ... */
        ];

        $('#tagcloud').jQCloud(words, {
            autoResize: true
        });
        }
    );