package models;

import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.List;

@Entity
public class Answer {
    @Id
    @GeneratedValue
    public int id;

    @Constraints.Required
    public Integer quesId;
    public String response;


    public String date;
    public String username;
    public Integer userId;
    public Integer likes;
    public Integer dislikes;

    public Answer(){}
    public Answer(int id, int quesId, String response, String date, int likes, int dislikes, String username, int userId){
        this.id = id;
        this.response = response;
        this.quesId = quesId;
        this.date = null;
        this.likes = likes;
        this.dislikes = dislikes;
        this.username = username;
        this.userId = userId;
    }
    public void save() { JPA.em().persist(this);}

    public static Answer findAnswer(Integer id){
        return JPA.em().find(Answer.class, id);
    }

    public static List<Answer> findBestAnswersToQuestions(List<Question> allQuestions){
        TypedQuery<Answer> query;
        List<Answer> tempList = null;
        List<Answer> answersList = null;
        for(Question q: allQuestions){
            query = JPA.em().createQuery("SELECT m FROM Answer m WHERE m.quesId = :qId ORDER BY m.likes DESC", Answer.class);
            tempList = query.setParameter("qId",q.id).getResultList();
            answersList.add(tempList.get(0));
        }

        return answersList;
    }
    public static List<Answer> findAll() {
        TypedQuery<Answer> query = JPA.em().createQuery("SELECT m FROM Answer m",Answer.class);
        return query.getResultList();
    }

    public static List<Answer> findBestAnswers(){
        Query queryAns = JPA.em().createQuery("SELECT NEW models.Answer(n.id, n.quesId, n.response, n.date, MAX(n.likes), n.dislikes, n.username, n.userId) FROM Answer n GROUP BY n.quesId", Answer.class);
        return queryAns.getResultList();
    }

    public static List<Answer> findAllAnswerToThisQuestion(Integer quesId){
//        Query queryAns = JPA.em().createQuery("SELECT NEW models.Answer(n.id, n.quesId, n.response, n.date, MAX(n.likes), n.dislikes, n.username) FROM Answer n WHERE n.quesId= :qId", Answer.class);
        Query queryAns = JPA.em().createQuery("SELECT m FROM Answer m WHERE m.quesId = :qId",Answer.class);
        return queryAns.setParameter("qId", quesId).getResultList();
//        return queryAns.getResultList();
    }


    public void updateLikes(){

        this.likes += 1;
        this.save();
    }

    public void updateDislikes(){

        this.dislikes += 1;
        this.save();
    }



}


