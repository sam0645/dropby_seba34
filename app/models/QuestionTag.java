package models;

import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class QuestionTag {

    @Id
    @GeneratedValue
    Integer id;
    Integer quesId;
    Integer tagId;

    public void save(){
        JPA.em().persist(this);
    }


}
