package models;

import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Amit Sama on 04-Jun-15.
 */
@Entity
public class Tag {
    @Id
    @GeneratedValue
    public Integer id;
    public String name;
    Integer count;


    public static Tag _tagObject;

    public static Tag getTagObject(){
        if( _tagObject == null){
            _tagObject = new Tag();
        }
        return _tagObject;
    }

    public Integer save() {
        JPA.em().persist(this);
        return this.id;
    }


    public void updateTags(String []tags, Integer queId) {

        Integer [] newTagIds = {0,0,0,0,0};
        Integer counter =0;
        for (String tag: tags) {

            TypedQuery<Tag> query;
            query = JPA.em().createQuery("SELECT m FROM Tag m WHERE m.name = :taga", Tag.class);
            List<Tag> thisList = query.setParameter("taga", (String)tag).getResultList();
//            System.out.println(thisList.size());
//            System.out.println(thisList.isEmpty());


            if (thisList.isEmpty()){

                Tag obj = new Tag();
                obj.name = tag;
                obj.count = 1;
                newTagIds[counter++] = obj.save();

            }
            else {

                for (Tag tga: thisList){
                    tga.count +=1;
                    tga.save();
                }
            }

        }
        //Adding the new tagIds in the QuestionTag Table
        for (int x=0; x<counter;x++){
            QuestionTag obj1 = new QuestionTag();
            obj1.quesId = queId;
            obj1.tagId = newTagIds[x];
            obj1.save();
        }

    }
}
