package models;

import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.TypedQuery;
import java.util.List;


@Entity
public class User {
    @Id
    @GeneratedValue
    Integer id;
    public String username;
    public String email;
    public String pword;

    public void save() {
        JPA.em().persist(this);
    }

    public static User findUserByUsername(String username){
        TypedQuery<User> query = JPA.em().createQuery("SELECT m FROM User m where m.username = :uname",User.class);
        return query.setParameter("uname", username).getResultList().get(0);
    }

    public static Boolean doesEmailExists(String email){
        TypedQuery<User> query = JPA.em().createQuery("SELECT m FROM User m where m.email = :mail",User.class);
        return (!query.setParameter("mail",email).getResultList().isEmpty());
    }

    public static Boolean doesUsernameExists(String username){
        TypedQuery<User> query = JPA.em().createQuery("SELECT m FROM User m where m.username = :uname",User.class);
        return (!query.setParameter("uname",username).getResultList().isEmpty());
    }
}
