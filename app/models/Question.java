package models;

import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.List;
import javax.persistence.Entity;



@Entity
public class Question {
    @Id
    @GeneratedValue
    public int id;

    public int totalAnswers;
    @Constraints.Required //for thowing errors
    public String title;
    public String detail;
    public String username;
    public Integer userId;
    public String date;
    public Integer views;
    public Integer likes;

    public Integer save() {

        JPA.em().persist(this);
        return this.id;
    }

    public static Question findThisQuestion(Integer quesId){
        return JPA.em().find(Question.class, quesId);
    }

    public static List<Question> findAll(){
        TypedQuery<Question> query = JPA.em().createQuery("SELECT m FROM Question m ORDER BY date DESC", Question.class);
        return query.getResultList();
    }

    public void updateTotalAnswers(){

        this.totalAnswers += 1;
        this.save();
    }

    public void updateViews(){

        this.views += 1;
        this.save();
    }

    public void updateLikes(){

        this.likes += 1;
        this.save();
    }



    /**
     * Methods for adding views and likes to questions
     *
     */


}
