package controllers;

import models.Answer;
import models.Question;
import models.Tag;
import models.User;

import play.Routes;
import play.data.DynamicForm;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.libs.Crypto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static models.Question.findThisQuestion;
import static play.libs.Crypto.encryptAES;

public class AllQuestions extends Controller {

    public static Result index (){
        return ok(views.html.index.render());
    }
    public static Result registerUser(){
        return ok(views.html.registration.render("Welcome to DropBy"));
    }

    @Transactional
    public static Result addUser(){

        DynamicForm form = Form.form().bindFromRequest();
        String username = form.get("username");
        String password = form.get("password");
//        @Email String email;
        String email = form.get("email");
//        validation.email(email);
        System.out.println(email);
        System.out.println(username);
        System.out.println(password);
//        encryptAES(password, application.secret);

        String encryptedPassword;
        if (username.isEmpty()  || password.isEmpty() || email.isEmpty()){
            return ok (views.html.registration.render("All fields are compulsory. Please fill them again."));
        }

        else {
            if (User.doesEmailExists(email)){
                return ok(views.html.registration.render("Email is already registered"));
            }
            if (User.doesUsernameExists(username)){
                return ok(views.html.registration.render("Username is already used. Please choose another username"));
            }

//            encryptedPassword = bcrypt.hashpw(password,bcrypt.gensalt());
            encryptedPassword = encryptAES(password);
            User user = new User();
            user.email = email;
            user.username = username;
            user.pword = encryptedPassword;
            user.save();
            System.out.println(encryptedPassword);
            return ok(views.html.registrationSuccessful.render("Registration successful"));
        }
    }

    public static Result login(){
        return ok(views.html.login.render("Login to DropBy"));
    }

    @Transactional
    public static Result loginUser(){

        DynamicForm form = Form.form().bindFromRequest();
        String username = form.get("username");
        String password = form.get("password");
        System.out.println("In loginUser()" + username + " === " + password);
//        System.exit(33);
        User user  = User.findUserByUsername(username);
        if (user!= null && user.pword.equals(encryptAES(password))) {
            return redirect(controllers.routes.AllQuestions.showAll(user.username));
        }

        return ok(views.html.login.render("Invalid login credentials. Please try again."));
    }

    //@play.db.jpa.Transactional(readOnly = true)
    public static Result validateUser(String username){

        return redirect(controllers.routes.AllQuestions.showAll(username));
    }


    @play.db.jpa.Transactional(readOnly = true)
    public static Result showAll(String username) {

        List<Question> allQuestions = Question.findAll();
        List<Answer> ans = Answer.findBestAnswers();
        /*for (Answer a: ans){
            System.out.println(a.likes);
            System.out.println(a.response);
        }*/

        return ok(views.html.showAll.render(allQuestions, username, ans));
    }

    @play.db.jpa.Transactional
    public static Result showQuestion(Integer quesId,String quesTitle, String username){
        List<Answer> allAnswers = null;
        Question question = findThisQuestion(quesId);
        question.updateViews();
        allAnswers = Answer.findAllAnswerToThisQuestion(quesId);
        return ok(views.html.showQuestion.render(allAnswers, question, username));
    }

    @Transactional
    public static Result createQuestion(String username){
        DynamicForm form = Form.form().bindFromRequest();
        String quesTitle = form.get("title");
        String quesDetail = form.get("detail");
        String quesTags = form.get("tags");


        String tags[] = quesTags.split("\\s*\\,\\s*");

        /*for (String tg: tags) {
            String a = (String)tg;
            System.out.println(a);
        }
        System.exit(10);*/
        Integer quesId;
        DateFormat dateFormat_1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        //DateFormat dateFormat_2 = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        String quesDate = dateFormat_1.format(date);
        //System.out.println(dateFormat_1.format(date));

        Question question = new Question();
        question.title = quesTitle;
        question.detail = quesDetail;
        //question.tags = quesTags;
        question.date = quesDate;
        question.username = username;
        question.views = 1;
        question.likes = 0;

        quesId = question.save();

        //We need to update three table here 1) Tag 2) Ques_Tag 3) User
        Tag tg = new Tag();
        tg.updateTags(tags, quesId); // for updating the tag table

        flash("success","Question successfully created");
        return redirect(controllers.routes.AllQuestions.showAll(username));
        //return redirect(controllers.routes$.AllQuestions.addQuestion());
    }


    public static Result addQuestion(String username) {
        return ok(views.html.addNewQuestion.render(username));
    }


    //send the title of the question to the template
    public static Result addResponse(Integer ques_id, String title, String username) {

        return ok(views.html.addNewResponse.render(ques_id,title,username));
    }

    @play.db.jpa.Transactional
    public static Result createResponse(Integer ques_id, String username) {
        DynamicForm form = Form.form().bindFromRequest();
        String response = form.get("response");
        DateFormat dateFormat_1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String ansDate = dateFormat_1.format(date);
        Answer ans = new Answer();
        ans.quesId = ques_id;
        ans.response = response;
        ans.username = "suman";
        ans.date = ansDate;
        ans.likes = 0;
        ans.dislikes =0;
        Question question = Question.findThisQuestion(ques_id);
        question.updateTotalAnswers();
        String question_title = question.title;
        ans.save();
        flash("success","Response successfully added");
        //return redirect(controllers.routes.AllQuestions.showAll("suman"));
        return redirect(controllers.routes.AllQuestions.showQuestion(ques_id, question_title, "suman"));
    }


    @play.db.jpa.Transactional
    public static Result likeQuestion(Integer quesId){
        Question ques = Question.findThisQuestion(quesId);
        ques.updateLikes();
        ques.save();

        return ok("added like to quwst: "+ques.id);
    }

    @play.db.jpa.Transactional
    public static Result unlikeAnswer(Integer answID){
        Answer ans = Answer.findAnswer(answID);
        ans.updateDislikes();
        ans.save();

        return ok("added dislike to answ: "+ans.id);
    }

    @play.db.jpa.Transactional
    public static Result likeAnswer(Integer answID){
        Answer ans = Answer.findAnswer(answID);
        ans.updateLikes();
        ans.save();

        return ok("added like to answ: "+ans.id);
    }

    public static Result javascriptRoutes() {
        response().setContentType("text/javascript");
        return ok(
                Routes.javascriptRouter("jsRoutes",
                        controllers.routes.javascript.AllQuestions.likeAnswer(),
                        controllers.routes.javascript.AllQuestions.unlikeAnswer(),
                        controllers.routes.javascript.AllQuestions.likeQuestion()
                )
        );
    }
}
